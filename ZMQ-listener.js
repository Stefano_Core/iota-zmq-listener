const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')

//Initialize variables
const zmqNodeAddress = 'tcp://my-iota-node.com:5556'

//Open ZMQ communication channel with IOTA node and receive TX data
getTxFromZmq = () => {
  zmqSub.connect(zmqNodeAddress)
  //Subscribe to publication of specific messages
  //'tx' = New transactions propagated by the node
  //'sn' = New confirmed transactions (milestones)
  zmqSub.subscribe('tx')
  zmqSub.on('message', msg => {
      const data = msg.toString().split(' ') // Split to get topic & data
      // Use index 0 to match 'tx'
      switch ( data[0] ) {
          case 'tx':
            console.log(`NEW TX JUST ARRIVED: `)
            console.log()
            console.log(`TX HASH: ` + data[1])
            console.log(`Address: ` + data[2])
            console.log(`Timestamp: ` + new Date(data[5] * 1000))
            console.log(`Bundle HASH: ` + data[8])
            console.log(`Trunk HASH: ` + data[9])
            console.log(`Branch HASH: ` + data[10])
            console.log(`Tag: ` + data[12])
            console.log('Transaction link: https://thetangle.org/transaction/' + data[1] )
            console.log()
            console.log(`Full transaction data: `)
            console.log(data)
            console.log()
            console.log('-------------------------------------------------------------------')
          break
      }
       })
  }

//--------------------- START APPLICATION ---------------------
getTxFromZmq()